<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';
 
/**
 * Module information
 */
$aModule = array(
    'id'           => 'ocb_exception',
    'title'        => 'OXID Cookbook :: Display Exceptions',
    'description'  => 'Displays the exceptionlog in the backend of your shop.',
    'thumbnail'    => 'cookbook.jpg',
    'version'      => '1.0.2',
    'author'       => 'Joscha Krug & Konstantin Kuznetsov & Josef Puckl',
    'url'          => 'http://www.oxid-kochbuch.de',
    'email'        => 'support@marmalade.de',
    'extend'       => array(),
    'controllers' => array(
        'ocb_exception_main' => \OxidCommunity\OcbExceptions\Controller\Admin\Exceptions::class,
    ),

    'templates'    => array(
        'ocb_exception_main.tpl' => 'oxcom/ocbexceptions/views/admin/tpl/ocb_exception_main.tpl',
    ),
);