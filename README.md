OXID Cookbook :: Exceptions
=============================

Display Exceptions. For OXID eShop 6.1


### Module installation via composer

    composer require ecs/ocbexceptions

### Module activation

Activate module in shop admin backend.
